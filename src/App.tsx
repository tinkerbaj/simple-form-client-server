import Form from "./hooks/Form"
import Nav from "./hooks/Nav"
import Theme from "./hooks/Theme"

function App() {
  return (
    <>
      <Nav />
      <div className="App">
        <Form />
      </div>
    </>
  )
}

export default App
